---
number: "Project Based Learning"
title: "TPBL Allocation 0"
allocation: "Allocation 0"
version: 1
videoId: ""
videoTitle: ""
path: "/pbl/tokenomics/a0-pbl"
videos:
  [
  ]
fullWidthImageAndText:
  {
    src: "/keepbees.jpeg",
    title: "Project Based Learning",
    subtitle: "Allocation 0",
    orientation: "right",
  }
objectives:
  {
    title: "Goals",
    list:
      [
        "Write a goal here",
        "And another here",
        "More if needed...",
      ],
  }
recirc:
  { image: "/learn.jpeg", color: "bg-blue-600", list: ["a0-cardano-4-climate", "a0-littlefish-foundation"] }
---

## What we are doing

### Current Steps
- Each group can list current projects here

### Next Steps
- Upcoming work
- Help wanted / bounties

## How to get involved
- You can share links to resources
- Or meeting times

## A0 Voter Suggested Goals - Project Based Learning
- Promote GMBL for payment of project dev	Take the raw material of TPBL A0 and PPBL Course #1 and begin to write cohesive PBL Curriculum. Create systems so that other Groups can make rec's for Projects + Case Studies
- Will help when I can.	Create a cleaned-up and tighter curriculum for how to launch a new token.	Find out how to best structure accquried knowledge & materials to create new iterrations of our previous PBL courses.
- Create a revenue-generating product from PPBL
- have interesting experiments to share with the group, build learning material
- "Report how long A0 Project took and what was achieved.
- Report should be reusable in A1"