---
number: "Cardano4Climate"
title: "TPBL Allocation 0"
allocation: "Allocation 0"
version: 1
videoId: ""
videoTitle: ""
path: "/pbl/tokenomics/a0-cardano-4-climate"
videos:
  [
  ]
fullWidthImageAndText:
  {
    src: "/keepbees.jpeg",
    title: "Cardano4Climate Community Experiment",
    subtitle: "Allocation 0",
    orientation: "right",
  }
objectives:
  {
    title: "Goals",
    list:
      [
        "Write a goal here",
        "And another here",
        "More if needed...",
      ],
  }
recirc:
  { image: "/learn.jpeg", color: "bg-blue-600", list: ["a0-cardano-4-climate", "a0-littlefish-foundation"] }
---
## Making the World Work Better For All People, Animals and the Planet
## What we are doing

### Current Steps
- Each group can list current projects here

### Next Steps
- Upcoming work
- Help wanted / bounties

## How to get involved
- You can share links to resources
- Or meeting times

## A0 Voter Suggested Goals - Cardano 4 Climate
- Targetting Carbon Credits Issuance
- Report back to broader tokenomics team with recommendations for future Case Studies + TPBL Course Projects
- Incentivise real and verified impact metrics gathering
- Experiment with Gimbals to build community
- have interesting experiments to share with the group
- Onboarding people outside the crypto space to own Gimbals	Coomunity use case for inclusive, growing and engaging community
