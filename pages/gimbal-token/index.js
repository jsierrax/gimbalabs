import React from "react";
import Layout from "../../components/Layouts/Layout";
import GimbalTokenPage from "../../components/gimbaltoken/GimbalTokenPage";

export default function GimbalToken() {
  return (
    <Layout>
      <GimbalTokenPage />
    </Layout>
  );
}