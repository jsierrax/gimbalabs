import React from "react";
import Layout from "../../components/Layouts/Layout";
import StakePoolPage from "../../components/stakepools/StakePoolPage";

export default function Stakepools() {
  return (
    <Layout>
      <StakePoolPage />
    </Layout>
  );
}