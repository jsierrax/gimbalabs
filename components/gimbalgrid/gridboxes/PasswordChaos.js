import React from "react";

export default function PasswordChaos() {
  return (
    <a href="https://www.passwordchaos.io">
      <div className="bg-passcha bg-cover h-full flex flex-col p-6 justify-center items-center text-white">
        <h1 className="text-4xl">Password Chaos</h1>
        <h2>Evolving NFT project</h2>
      </div>
    </a>
  );
}
